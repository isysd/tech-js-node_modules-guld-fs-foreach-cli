#!/usr/bin/env node
const foreach = require('guld-fs-foreach')
const program = require('commander')
const { nodeSpawn } = require('guld-spawn')
const VERSION = require('./package.json').version

/* eslint-disable no-console */
program
  .name('guld-fs-foreach')
  .usage('<dir> <command> [flags...]', 'Run command for each file and/or directory in the given directory.')
  .description('Run command for each file and/or directory in the given directory.')
  .version(VERSION)
  .action(async (dir, c, flags) => {
    return foreach(dir, async f => {
      if (f === '') return
      nodeSpawn(c, undefined, [f]).then(console.log).catch(e => {
        console.error(e)
        process.exit(1)
      })
    }, flags.rawArgs.slice(4)).catch(e => {
      console.error(e)
      process.exit(1)
    })
  })

program.parse(process.argv)
